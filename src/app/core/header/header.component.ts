import { Component, OnInit} from '@angular/core';
import { ScoretrackerService } from 'src/app/scoretracker.service';
import { Team } from 'src/app/models/team';

@Component({
  selector: 'st-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  id: number;
  teams: Team[];
  
  constructor(private stService: ScoretrackerService) {}
  
  ngOnInit(): void {
    this.stService.id$.subscribe(async data => {
      this.id = await data;
      if(this.id){
        this.stService.getTeamsOfSeason(this.id).subscribe(teams => this.teams = teams);
      }
    });
  }

}
