export interface Season {
	id: number;
    number: number;
    division: boolean;
    actual: boolean;
    aborted: boolean;
    league: {
        id: number;
        name: string;
        address: string
    };
    periodDuration: number
}