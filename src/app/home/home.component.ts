import { Component, OnInit } from '@angular/core';
import { Season } from '../models/season';
import { ScoretrackerService } from '../scoretracker.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  seasons: Season[];

  constructor(private stService: ScoretrackerService) { }

  ngOnInit(): void {
    this.stService.getSeasons().subscribe(response => this.seasons = response);
  }

}
