import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FixuresComponent } from './fixures.component';

describe('FixuresComponent', () => {
  let component: FixuresComponent;
  let fixture: ComponentFixture<FixuresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FixuresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FixuresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
