import { Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ScoretrackerService } from '../scoretracker.service';
import { Season } from '../models/season';
import { Team } from '../models/team';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  seasons: [];
  id: number;
  season: Season;
  teams: Team[];
 
  constructor(private router :Router, private route: ActivatedRoute, private stService: ScoretrackerService) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.stService.getSeasonId(this.id);
    this.stService.getSeasonTable(this.id).subscribe( table => this.seasons = table);
    this.stService.getOneSeason(this.id).subscribe( season => {this.season = season},
      (error) => {
        this.router.navigate(['/error/not-found']);
    });
    this.stService.getTeamsOfSeason(this.id).subscribe(teams => this.teams = teams);
  }

}
