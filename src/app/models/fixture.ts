export interface Fixture {
    id: number;
    dateOfMatch: string;
    status: string;
    delegate: number;
    home: {
        id: number;
        name: string;
        address: string;
        deleted: false
        color1: string;
        color2: string;
        hasLogo: boolean;
    }
    visitor: {
        id: number;
        name: string;
        address: string;
        deleted: boolean;
        color1: string;
        color2: string;
        hasLogo: boolean; 
    }   
    homeGoals: number;
    visitorGoals: number;
    leagueId: number;
    season: {
        id: number;
        number: number;
        division: number;
        actual: boolean;
        locked: boolean;
        aborted: boolean;
        periodDuration: number;
    }
}