import { Fixture } from './fixture';

export interface Round {
    id: number;
    number: number;
    fixtures: Array<Fixture>;
}