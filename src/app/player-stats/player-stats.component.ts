import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ScoretrackerService } from '../scoretracker.service';
import { Season } from '../models/season';
import { PlayerState } from '../models/playerState';
import { Team } from '../models/team';

@Component({
  selector: 'app-player-stats',
  templateUrl: './player-stats.component.html',
  styleUrls: ['./player-stats.component.scss']
})
export class PlayerStatsComponent implements OnInit {
  id: number;
  season: Season;
  playersState: PlayerState;
  teams: Team[];
  constructor(private router :Router, private route: ActivatedRoute, private stService: ScoretrackerService) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.stService.getSeasonId(this.id);
    this.stService.getOneSeason(this.id).subscribe( season => {this.season = season},
      (error) => {
        this.router.navigate(['/error/not-found']);
    });
    this.stService.getPlayersTable(this.id).subscribe(players => this.playersState = players);
    this.stService.getTeamsOfSeason(this.id).subscribe(teams => this.teams = teams);
  }

}
