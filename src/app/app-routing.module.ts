import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { WrongRouteComponent } from './core/wrong-route/wrong-route.component';
import { TableComponent } from './table/table.component';
import { FixuresComponent } from './fixures/fixures.component';
import { PlayerStatsComponent } from './player-stats/player-stats.component';
import { FixtureComponent } from './fixture/fixture.component';


const routes: Routes = [
  {path: "home", component: HomeComponent},
  {path: "season/:id/table", component: TableComponent},
  {path: "season/:id/fixtures", component: FixuresComponent},
  {path: "season/:id/fixtures/fixture/:fictureId", component: FixtureComponent},
  {path: "season/:id/players-stats", component: PlayerStatsComponent},
  {path: "error/not-found", component: WrongRouteComponent},
  {path: "", component: HomeComponent, pathMatch:"full"},
  {path: "**", component: WrongRouteComponent, pathMatch:"full"}
  ];
  

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
