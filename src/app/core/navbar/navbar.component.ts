import { Component, OnInit} from '@angular/core';
import { Team } from 'src/app/models/team';
import { ScoretrackerService } from 'src/app/scoretracker.service';
import { Season } from 'src/app/models/season';

@Component({
  selector: 'st-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  id: number;
  teams: Team[];
  season: Season;
 
  constructor(private stService: ScoretrackerService) {}
  
  ngOnInit(): void {
    this.stService.id$.subscribe(async data => {
      this.id = await data;
      if(this.id){
        this.stService.getTeamsOfSeason(this.id).subscribe(teams => this.teams = teams);
        this.stService.getOneSeason(this.id).subscribe( season => this.season = season);
      }
    });
  }
  
}
