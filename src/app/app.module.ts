import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { TableComponent } from './table/table.component';
import { FixuresComponent } from './fixures/fixures.component';
import { PlayerStatsComponent } from './player-stats/player-stats.component';
import { HeaderComponent } from './core/header/header.component';
import { NavbarComponent } from './core/navbar/navbar.component';
import { WrongRouteComponent } from './core/wrong-route/wrong-route.component';
import { HttpClientModule } from '@angular/common/http';
import { FixtureComponent } from './fixture/fixture.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TableComponent,
    FixuresComponent,
    PlayerStatsComponent,
    HeaderComponent,
    NavbarComponent,
    WrongRouteComponent,
    FixtureComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
