import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { Season } from './models/season';
import { Team } from './models/team';
import { Round } from './models/round';
import { PlayerState } from './models/playerState';
import { FixtureComponent } from './fixture/fixture.component';
import { Fixture } from './models/fixture';

const BASE_URL = "http://173.249.5.46:8080/scoretracker-0.1"

@Injectable({
  providedIn: 'root'
})
export class ScoretrackerService {
  id$: Observable<any>;
  private getId = new BehaviorSubject(this.id$);
  
  constructor(private http: HttpClient) {
    this.id$ = this.getId.asObservable();
  }

  getSeasonId(data) {
    this.getId.next(data);
  }

  getSeasons() :Observable<Season[]>{
  return this.http.get<Season[]>(`${BASE_URL}/api/seasons/public`)
  }

  getSeasonTable(id: number) :Observable<any> {
    return this.http.get(`${BASE_URL}/api/seasons/public/table/${id}`)
  }

  getOneSeason(id: number) :Observable<Season> {
    return this.http.get<Season>(`${BASE_URL}/api/seasons/public/${id}`)
  }

  getTeamsOfSeason(id: number) :Observable<Team[]> {
    return this.http.get<Team[]>(`${BASE_URL}/api/teams/public/season/${id}`)
  }
  getFixtures(seasonId: number) :Observable<Round[]> {
    return this.http.get<Round[]>(`${BASE_URL}/api/rounds/public/season/${seasonId}`)
  }

  getPlayersTable(seasonId: number) :Observable<PlayerState> {
    return this.http.get<PlayerState>(`${BASE_URL}/api/seasons/public/players-table/${seasonId}`)
  }

  getFixTimeline(fixtureId: number) :Observable<any> {
    return this.http.get(`${BASE_URL}/api/actions/public/fixture/${fixtureId}`)
  }

  getFixStats(fixtureId: number) :Observable<any> {
    return this.http.get(`${BASE_URL}/api/fixtures/public/stats/${fixtureId}`)
  }

  getFixture(fixtureId: number) :Observable<Fixture> {
    return this.http.get<Fixture>(`${BASE_URL}/api/fixtures/public/${fixtureId}`)
  }
  
}
