import { Component, OnInit } from '@angular/core';
import { ScoretrackerService } from '../scoretracker.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Season } from '../models/season';
import { Fixture } from '../models/fixture';
import { Team } from '../models/team';
import { interval } from 'rxjs/internal/observable/interval';
import { startWith, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-fixture',
  templateUrl: './fixture.component.html',
  styleUrls: ['./fixture.component.scss']
})
export class FixtureComponent implements OnInit {
  id: number;
  fixtureId: number;
  season: Season;
  fixture: Fixture;
  fixtureTlHalfTime: any;
  fixtureTlFullTime: any;
  fixtureTimeUp: any;
  fixtureStats: any;
  teams: Team[];

  constructor(private router: Router, private route: ActivatedRoute, private stService: ScoretrackerService) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.stService.getSeasonId(this.id);
    this.fixtureId = this.route.snapshot.params.fictureId;
    this.stService.getOneSeason(this.id).subscribe(season => 
      {this.season = season},
      (error) => {
        this.router.navigate(['/error/not-found']);
    });
   //get fixture in every 60sek
    interval(60000)
    .pipe(
      startWith(0),
      switchMap(() =>  this.stService.getFixture(this.fixtureId))
    )
    .subscribe(fixture => this.fixture = fixture);

    //get fixture timeline in every 60sek
    interval(60000)
    .pipe(
      startWith(0),
      switchMap(() =>  this.stService.getFixTimeline(this.fixtureId))
    )
    .subscribe(timeline => {
      const fixtureTimeLine = timeline.filter(el => el.type !== "PLAY");
      fixtureTimeLine.sort((a,b) => a.minute > b.minute);
      this.fixtureTlHalfTime = fixtureTimeLine.filter(el => el.minute <= 45);
      this.fixtureTlFullTime = fixtureTimeLine.filter(el => el.minute > 45);
      this.fixtureTimeUp = timeline.filter(el => el.type === "PLAY");
    });
    //get fixture stats in every 60sek
    interval(60000)
    .pipe(
      startWith(0),
      switchMap(() =>  this.stService.getFixStats(this.fixtureId))
    )
    .subscribe(stats => this.fixtureStats = stats);
    
    this.stService.getTeamsOfSeason(this.id).subscribe(teams => this.teams = teams);
  }

}
