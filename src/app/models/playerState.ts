import { Team } from './team'

export interface PlayerState {

    id: number;
    player: {
        id: number;
        firstName: string;
        lastName: string;
        team: Team;
    }

    season: {
        id: number;
        number: number;
        division: number;
        actual: boolean;
        locked: boolean;
        aborted: boolean;
        periodDuration: number;
    }
    played: number;
    goals: number;
    assists: number;
    yellow: number;
    red: number;
    suspension2Min: number;
}