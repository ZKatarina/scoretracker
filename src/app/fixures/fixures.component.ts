import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ScoretrackerService } from '../scoretracker.service';
import { Season } from '../models/season';
import { Team } from '../models/team';
import { Round } from '../models/round';
import { interval } from 'rxjs/internal/observable/interval';
import { startWith, switchMap } from 'rxjs/operators';


@Component({
  selector: 'app-fixures',
  templateUrl: './fixures.component.html',
  styleUrls: ['./fixures.component.scss']
})
export class FixuresComponent implements OnInit {
  id: number;
  season: Season;
  rounds: Round[];
  teams: Team[];
  
  constructor(private router :Router, private route: ActivatedRoute, private stService: ScoretrackerService) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.stService.getSeasonId(this.id);
    this.stService.getOneSeason(this.id).subscribe(season => {this.season = season},
      (error) => {
        this.router.navigate(['/error/not-found']);
    });
    this.stService.getTeamsOfSeason(this.id).subscribe(team => this.teams = team);
    interval(60000)
    .pipe(
      startWith(0),
      switchMap(() =>  this.stService.getFixtures(this.id))
    )
    .subscribe(rounds => this.rounds = rounds);
  }
  
}