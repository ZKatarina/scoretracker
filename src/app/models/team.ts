export interface Team {
    id: number;
    name: string;
    address: string;
    deleted: boolean;
    color1: string;
    color2: string;
    hasLogo: boolean;
}